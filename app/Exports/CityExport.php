<?php

namespace App\Exports;

use App\Models\City;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CityExport implements FromQuery, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return City::query();
    }

    public function headings(): array
    {
        return [
            'Cod',
            'Name',
        ];
    }

    public function map($city): array
    {
        return [
            $city->cod,
            $city->name,
        ];
    }

}
