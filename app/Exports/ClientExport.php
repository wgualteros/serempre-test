<?php

namespace App\Exports;

use App\Models\Client;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ClientExport implements FromQuery, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return Client::query();
    }

    public function headings(): array
    {
        return [
            'Cod',
            'Name',
            'Cod_city',
        ];
    }

    public function map($client): array
    {
        return [
            $client->cod,
            $client->name,
            $client->city->cod
        ];
    }
}
