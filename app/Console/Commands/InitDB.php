<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class InitDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serempre:initdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations and call model factories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('This operation will delete all existing data. Do you want to continue?', true)) {
            try
            {
                $this->info('Executing migrations');
                $this->call("migrate:refresh");
                $this->info('Executing Seeder');
                $this->call("db:seed");
                $this->info('Database initialized successfully');
                return Command::SUCCESS;
            }
            catch(Exception $ex)
            {
                $this->error($ex->getMessage());
                return Command::FAILURE;
            }
        }
    }
}
