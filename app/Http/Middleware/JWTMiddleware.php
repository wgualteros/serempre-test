<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try
        {
            $user = JWTAuth::parseToken()->authenticate();
        }
        catch(TokenInvalidException $e)
        {
            return response()->json(["msg" => "Token inválido"], 401);
        }
        catch(TokenExpiredException $e)
        {
            return response()->json(["msg" => "Token expirado"], 403);
        }
        catch(JWTException $e)
        {
            return response()->json(["msg" => "Token no encontrado"], 403);
        }
        return $next($request);
    }
}
