<?php

namespace App\Http\Controllers;

use App\Exports\CityExport;
use App\Exports\ClientExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function exportCities()
    {
        return Excel::download(new CityExport, 'Cities-exported.xlsx');
    }

    public function exportClients()
    {
        return Excel::download(new ClientExport, 'Clients-exported.xlsx');
    }
}
