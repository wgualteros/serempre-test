<?php

namespace App\Http\Controllers;

use App\Imports\CityImport;
use App\Imports\ClientImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function importCitiesForm(Request $request)
    {
        return view('city.import-form');
    }

    public function importCities(Request $request)
    {
        Excel::import(new CityImport, $request->file);
        return redirect('cities')->with('user-message', 'Importación realizada exitosamente');;
    }

    public function importClientsForm(Request $request)
    {
        return view('client.import-form');
    }

    public function importClients(Request $request)
    {
        Excel::import(new ClientImport, $request->file);
        return redirect('clients')->with('user-message', 'Importación realizada exitosamente');;
    }

}
