<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['cities'] = City::paginate(20);
        return view("city.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("city.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'cod' => 'required|string|max:10|unique:App\Models\City,cod',
            'name' => 'required|string|max:100'
        ];
        $messages = [
            'cod.required' => 'El código de ciudad es requerido',
            'cod.unique' => 'El código de ciudad ya ha sido asignado a otra ciudad',
            'name.required' => 'El nombre es requerido',
        ];

        $this->validate($request, $fields, $messages);

        $receivedData = $request->except('_token');
        City::insert($receivedData);
        return redirect("cities")->with('user-message', 'Ciudad agregada exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $city = City::findOrFail($id);
        return view("city.edit", compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'cod' => 'required|string|max:10',
            'name' => 'required|string|max:100'
        ],
        [
            'cod.unique' => 'El código de ciudad ya ha sido asignado a otra ciudad',
            'cod.required' => 'El código de ciudad es obligatorio',
            'name.required' => 'El nombre es obligatorio',
        ]);

        $city = City::findOrFail($id);
        $validator->sometimes('cod', 'unique:App\Models\City,cod', function ($input) use($city) {
            return $input->cod != $city->cod;
        });

        $validator->validate($request);
        $receivedData = $request->except(['_token', '_method']);
        City::where('id', '=', $id)->update($receivedData);

        //return view("city.edit", compact('city'));
        return redirect("cities")->with('user-message', 'Ciudad modificada exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::destroy($id);
        return redirect('cities')->with('user-message', 'Ciudad eliminada exitosamente');;
    }
}
