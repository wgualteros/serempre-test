<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has("id_city"))
        {
            $idCity = $request->get("id_city");
        }
        else
        {
            $idCity = '000';
        }

        if($idCity === '000')
        {
            $data['clients'] = Client::paginate(20);
        }
        else
        {
            $data['clients'] = Client::where('id_city', $idCity)->paginate(15);
        }
        $data['cities'] = City::all();
        $data['id_city'] = $idCity;
        return view("client.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['cities'] = City::all();
        $data['id_city'] = City::first()->id;
        return view("client.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'cod' => 'required|string|max:10|unique:App\Models\Client,cod',
            'name' => 'required|string|max:100',
            'id_city' => 'required',
        ];
        $messages = [
            'cod.required' => 'El código de cliente es requerido',
            'cod.unique' => 'El código de cliente ya ha sido asignado a otra cliente',
            'name.required' => 'El nombre es requerido',
            'id_city.required' => 'La ciudad es obligatoria',
        ];
        $this->validate($request, $fields, $messages);


        $newCilent = new Client();
        $newCilent->cod = $request->get('cod');
        $newCilent->name = $request->get('name');


        $idCity = $request->get('id_city');
        $foundCity = City::findOrFail($idCity);
        $newCilent->city()->associate($foundCity);
        $newCilent->save();

        return redirect("clients")->with('user-message', 'Cliente agregado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['cities'] = City::all();
        $foundClient = Client::findOrFail($id);
        $data['client'] = $foundClient;
        $data['id_city'] = $foundClient->city->id;
        return view("client.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'cod' => 'required|string|max:10',
            'name' => 'required|string|max:100',
            'id_city' => 'required|string|max:100',
        ],
        [
            'cod.unique' => 'El código de ciudad ya ha sido asignado a otra ciudad',
            'cod.required' => 'El código de ciudad es obligatorio',
            'name.required' => 'El nombre es obligatorio',
            'id_city.required' => 'La ciudad es obligatoria',
        ]);
        $foundClient = Client::findOrFail($id);
        $validator->sometimes('cod', 'unique:App\Models\Client,cod', function ($input) use($foundClient) {
            return $input->cod != $foundClient->cod;
        });
        $validator->validate($request);

        $foundClient->cod = $request->get('cod');
        $foundClient->name = $request->get('name');
        $idCity = $request->get('id_city');
        $foundCity = City::findOrFail($idCity);
        $foundClient->city()->associate($foundCity);
        $foundClient->save();
        return redirect("clients")->with('user-message', 'Cliente modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::destroy($id);
        return redirect('clients')->with('user-message', 'Cliente eliminado exitosamente');;
    }
}
