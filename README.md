Serempre Test
Herramienta de gestión de clientes de prueba.



-----------------------------------------------------------------------------
-------------------------------Requerimientos--------------------------------
-----------------------------------------------------------------------------
Se debe tener instalados los siguientes programas:

Git
Composer
Nodejs
Servidor web
PHP <= 7.3
Base de datos MariaDB o MySQL
(Opcional) PHPMyAdmin u otra herramienta gráfica para la gestiónd e bases de datos MySQL/MariaDB


Verificar que estén instaladas y habilitadas las extensiones de php:

php-mbstring
php-zip
php-xml
php-gd2

Verificar las siguientes configuraciones de php, en el archivo php.ini

max_execution_time = -1
max_input_time = -1
post_max_size = 100M
upload_max_filesize = 100M

Estas configuraciones permiten subir archivos de gran tamaño

-----------------------------------------------------------------------------
-------------------------------Instalación--------------------------------
-----------------------------------------------------------------------------

Clonar repositorio (git requerido)
git clone https://gitlab.com/wgualteros/laravel-test.git

ingresar al directorio
cd laravel-test

e ingresar los siguientes comandos:

composer install
npm install
npm run dev

CREAR BASE DE DATOS

Configurar el archivo .env con los datos de configuración de la base de datos.

Ejecutar migraciones y seeders con el siguiente comando personalizado

php artisan serempre:initdb

Esto creará dos usuarios. Para ambos usuarios la contraseña estará fija con el valor "password"
También se crearán 50 ciudades y 10 clientes para cada ciudad

EJECUTAR

php artisan serve
Ir a un navegador e introducir la siguiente URL:

http://127.0.0.1:8000


-----------------------------------------------------------------------------
-------------------------------Rutas-----------------------------------------
-----------------------------------------------------------------------------
/login: Inicio de sesión (requerido apra acceder a als demás rutas)
/home: página principal

CRUD CIUDADES
/cities: lista de ciudades 
/cities/create: Formulario para crear una nueva ciudad
/cities/{id}/edit: Formulario para editar una ciudad que existe previamente
/cities/import: Formulario para cargar archivo excel con ciudades para importar
/cities/export: Al ir a esta ruta se descargará una rchivo excel con la ista de ciudades

CRUD CLIENTES
/clients: lista de clientes
/clients/create: Formulario para crear un nuevo cliente
/clients/{id}/edit: Formulario para editar una ciudad que existe previamente
/clients/import: Formulario para cargar archivo excel con clientes para importar
/clients/export: Al ir a esta ruta se descargará una rchivo excel con la ista de clientes

CRUD USUARIOS
/users: lista de ciudades 
/users/create: Formulario para crear una nueva ciudad
/users/{id}/edit: Formulario para editar una ciudad que existe previamente


-----------------------------------------------------------------------------
------------------Formato de Importación/Exportación------------------------
-----------------------------------------------------------------------------
El formato para los archivos de importación y exportación es el mismo

El archivo Excel que contiene las ciudades tiene dos columnas llamadas Cod y Name
El archivo Excel que contiene los clientes tiene tres columnas llamadas Cod, Name y Cod_City

Nota: en estos archivos no están presentes los identificadores generados como clave primaria en la base de dato. La asociación entre los clinentes y la ciudad a la que corresponden se hace a través del código de ciudad, sin embargo en la base de datos esta asociación sí se efectúa a través de la clave primaria

*******************************API REST/JWT***********************************

------------------------------------------------------------------------------------
------------------------------------ENCABEZADOS-------------------------------------
------------------------------------------------------------------------------------
Para todas las rutas, usar los siguientes enxabezados:

Content-Type = application/json
Accept = application/json

Para todas las peticiones, con excepción de login usar el token devuelto al iniciar sesión
Authorization = Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYzNDI3MjA3MywiZXhwIjoxNjM0Mjc1NjczLCJuYmYiOjE2MzQyNzIwNzMsImp0aSI6IlRNcW5ldXVJd2pnQXJPZnYiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.Ofrxd4S_Zi2wIGhGIGgRIYEhpZzQEzzA84SI_izXh7o


------------------------------------------------------------------------------------
------------------------------------RUTAS-------------------------------------------
------------------------------------------------------------------------------------

POST /api/login: iniciar sesión

PETICIÓN
{
    "email": "user@unknown.com",
    "password": "password"
}

RESPUESTA

{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYzNDI3MTc1MiwiZXhwIjoxNjM0Mjc1MzUyLCJuYmYiOjE2MzQyNzE3NTIsImp0aSI6InhUR3FOSk1XRlRXYmV6NnciLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.aIQQLaIFqkyASAadtxlT0m5aAJk8G3RhnfxYzoHIFq4",
    "token_type": "bearer",
    "expires_in": 3600
}


POST /api/logout POST: cerrar sesión
RESPUESTA
{
    "message": "Successfully logged out"
}

RESPUESTA
{
    "id": 1,
    "name": "Dr. Lisa Langworth",
    "email": "koch.annabell@example.org",
    "email_verified_at": "2021-10-14T15:11:00.000000Z",
    "created_at": "2021-10-14T15:11:00.000000Z",
    "updated_at": "2021-10-14T15:11:00.000000Z"
}

POST /api/refresh: refrescar token

RESPUESTA
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTYzNDI3MTc1MiwiZXhwIjoxNjM0Mjc1MzUyLCJuYmYiOjE2MzQyNzE3NTIsImp0aSI6InhUR3FOSk1XRlRXYmV6NnciLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.aIQQLaIFqkyASAadtxlT0m5aAJk8G3RhnfxYzoHIFq4",
    "token_type": "bearer",
    "expires_in": 3600
}

GET /api/user/info: obtener información del usuario autenticado

RESPUESTA
{
    "id": 1,
    "name": "Dr. Lisa Langworth",
    "email": "koch.annabell@example.org",
    "email_verified_at": "2021-10-14T15:11:00.000000Z",
    "created_at": "2021-10-14T15:11:00.000000Z",
    "updated_at": "2021-10-14T15:11:00.000000Z"
}

POST /api/user/info: cambiar el nombre del usuario autenticado

------------------------------------------------------------------------------------
------------------------------------ERRORES-------------------------------------------
------------------------------------------------------------------------------------
Cödigo de respuesta: 401 Unauthorized
Contenido:
{
    "msg": "Token expirado"
}


Cödigo de respuesta: 403 Forbidden
Contenido:
{
    "msg": "Token no encontrado"
}

Cödigo de respuesta: 403 Forbidden
Contenido:
{
    "msg": "Token inválido"
}


*******************REGISTRO/NOTIFICACIÓN COREO ELECTRÓNICO************************
Driver: base de datos

Ir al archivo .env y configurar la cola y el correo electrónico, tal como se muestra a continuación:

QUEUE_CONNECTION=database

...

MAIL_MAILER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME="mygmail@gmail.com"
MAIL_PASSWORD="mypassword"
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS="mygmail@gmail.com"
MAIL_FROM_NAME="Online Web Tutor"

------------------------------------------------------------------------------------
------------------------------------RUTAS-------------------------------------------
------------------------------------------------------------------------------------






