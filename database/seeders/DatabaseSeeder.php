<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Client;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Step 1. Creating users');
        $users = User::factory()->count(2)->create();
        $this->command->info('Users created successfully');

        $this->command->info('Step 2. Creating cities and clients');
        $cities = City::factory()
            ->count(50)
            ->has(Client::factory()->count(10))
            ->create();
        $this->command->info('Cities and clients created successfully');
    }
}
