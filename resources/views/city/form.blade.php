@if($mode=='edit')
    <h2 class="text-center">Editar Ciudad</h2>
@elseif ($mode=='create')
    <h2 class="text-center">Crear Ciudad</h2>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)
            <li>
            {{$error}}
            </li>
        @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    <label for="cod">Código de Ciudad</label>
    <input
        type="text"
        name="cod"
        class="form-control"
        value="{{isset($city->cod)?$city->cod:old('cod')}}"
    >
</div>
<div class="form-group">
    <label for="name">Nombre de la Ciudad</label>
    <input
        type="text"
        name="name"
        class="form-control"
        value="{{ isset($city->name)?$city->name:old('name')}}"
    >
</div>
@if($mode=='edit')
    <input type="submit" value="Guardar Cambios" class="btn btn-success">
@elseif ($mode=='create')
    <input type="submit" value="Crear Ciudad" class="btn btn-success">
@endif
<a href="{{url('cities')}}" class="btn btn-primary">Regresar</a>
