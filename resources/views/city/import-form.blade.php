@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{url('cities/import')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="file">Archivo de Datos</label>
                <input type="file" name="file" id="file" accept=".xls, .xlsx" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Importar</button>
            <a href="{{url('cities')}}" class="btn btn-danger">Cancelar</a>
        </form>
    </div>
@endsection
