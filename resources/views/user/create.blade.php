@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Crear Usuario') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('/users') }}" enctype="multipart/form-data">
                        @csrf
                        @include('user.form', ['mode' => 'create'])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
