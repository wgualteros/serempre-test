<!DOCTYPE html>
<html>
    <head>
        <title>Test Email</title>
    </head>
    <body>
        <h1>Registro Exitoso</h1>
        <p>Por favor establezca su contraseña en el siguiente enlace</p>
        <a href="{{ url('users/' . $userId . '/resetPassword') }}">Establecer Contraseña</a>
    </body>
</html>
