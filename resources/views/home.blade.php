@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('SISTEMA DE GESTIÓN DE CLIENTES') }}</div>

                <div class="card-body">
                    <div>
                        <a href="{{url('cities')}}">Gestionar Ciudades</a>
                    </div>
                    <div>
                        <a href="{{url('clients')}}">Gestionar Clientes</a>
                    </div>
                    <div>
                        <a href="{{url('users')}}">Gestionar Usuarios</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
