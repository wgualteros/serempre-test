@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{url('clients/import')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="file">Archivo de Datos</label>
                <input type="file" name="file" id="file" accept=".xls, .xlsx">
            </div>
            <button type="submit" class="btn btn-success">Importar</button>
            <a href="{{url('clients')}}" class="btn btn-danger">Cancelar</a>
        </form>
    </div>
@endsection
