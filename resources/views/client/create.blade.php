@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{url("/clients")}}" method="post">
            @csrf
            @include('client.form', ['mode' => 'create'])
        </form>
    </div>
@endsection
