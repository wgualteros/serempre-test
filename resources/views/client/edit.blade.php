@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{url('clients/' . $client->id )}}" method="post">
            @csrf
            {{ method_field('PATCH')}}
            @include('client.form', ['mode' => 'edit'])
        </form>
    </div>
@endsection
