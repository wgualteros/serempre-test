@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has("user-message"))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ Session::get("user-message") }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <h2 class="text-center">Lista de Clientes</h2>
        <div class="row">
            <div class="col-6">
                <a href="{{url('clients/create')}}" class="btn btn-success mb-3">Crear cliente</a>
                <a href="{{url('clients/import')}}" class="btn btn-info mb-3">Importar</a>
                <a href="{{url('clients/export')}}" class="btn btn-primary mb-3">Exportar</a>
            </div>
            <div class="col-6">
                <form class="form-inline float-right">
                    <div class="form-group">
                        <label for="id_city">Buscar por Ciudad</label>
                        <select name="id_city" id="id_city" class="form-control mx-3" >
                            @if($id_city == "000")
                                <option selected value="000">{{__("<<<Todas>>>")}}</</option>
                            @else
                                <option value="000">{{__("<<<Todas>>>")}}</</option>
                            @endif
                            @foreach ($cities as $city)
                                @if($id_city == $city->id)
                                    <option selected value="{{$city->id}}">{{$city->cod. ' - ' . $city->name}}</option>
                                @else
                                    <option value="{{$city->id}}">{{$city->cod. ' - ' . $city->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Buscar">
                </form>
            </div>
        </div>
        {{ $clients->links() }}
        <table class="table table-bordered table-danger table-hover table-active text-center">
            <thead  class="thead-dark">
                <tr>
                    <th>Id</th>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Ciudad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                <tr>
                    <th scope="row" class="align-middle">{{$client->id}}</th>
                    <td class="align-middle">{{$client->cod}}</td>
                    <td class="align-middle">{{$client->name}}</td>
                    <td class="align-middle">{{$client->city->cod . " - " . $client->city->name}}</td>
                    <td>
                        <a href="{{url('clients/' . $client->id . '/edit')}}" class="btn btn-info">Editar</a>
                        <form
                            action="{{url('clients/' . $client->id)}}"
                            method="post"
                            class="d-inline"
                        >
                        @csrf
                        {{ method_field('DELETE')}}
                        <input
                            class="btn btn-danger"
                            type="submit"
                            value="Elminar"
                            onclick="return confirm('¿Esá seguro de que quiere eliminar el cliente?');"
                        >
                        </form>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $clients->links() }}
    </div>
@endsection
