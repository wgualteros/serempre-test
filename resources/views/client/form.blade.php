@if($mode=='edit')
    <h2>Editar Cliente</h2>
@elseif ($mode=='create')
    <h2>Crear Cliente</h2>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)
            <li>
            {{$error}}
            </li>
        @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    <label for="cod">Código de Cliente</label>
    <input
        type="text"
        name="cod"
        class="form-control"
        value="{{isset($client->cod)?$client->cod:old('cod')}}"
    >
</div>
<div class="form-group">
    <label for="name">Nombre del Cliente</label>
    <input
        type="text"
        name="name"
        class="form-control"
        value="{{ isset($client->name)?$client->name:old('name')}}"
    >
</div>

<div class="form-group">
    <label for="id_city">Ciudad</label>
    <select name="id_city" id="id_city" class="form-control">
        @foreach ($cities as $city)
            @if($id_city == $city->id)
                <option selected value="{{$city->id}}">{{$city->cod. ' - ' . $city->name}}</option>
            @else
                <option value="{{$city->id}}">{{$city->cod. ' - ' . $city->name}}</option>
            @endif
        @endforeach
    </select>
</div>
@if($mode=='edit')
    <input type="submit" value="Guardar Cambios" class="btn btn-success">
@elseif ($mode=='create')
    <input type="submit" value="Crear Cliente" class="btn btn-success">
@endif
<a href="{{url('clients')}}" class="btn btn-primary">Regresar</a>
